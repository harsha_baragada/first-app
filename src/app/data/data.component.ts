import { TransferService } from './../service/transfer.service';
import { DataService } from './../service/data.service';
import { Route } from '@angular/compiler/src/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css'],
})
export class DataComponent implements OnInit, OnDestroy {
  users!: any[];
  fruits!: any[];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private transferSevice: TransferService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => console.log(params['id']));
    this.dataService.getData().subscribe((data) => {
      this.users = data;
      console.log(data);
    });
    this.fruits = this.transferSevice.getData();
  }

  goToHome(userId: any) {
    this.router.navigate(['/feature/user', { id: userId }]);
  }

  ngOnDestroy() {}
}
