import { Injectable } from '@angular/core';

@Injectable()
export class TransferService {
  data: any;

  getData() {
    return this.data;
  }

  setData(data: any) {
    this.data = data;
  }
}
