import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
} from '@angular/core';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css'],
})
export class TemplateFormComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy {
  @Input() fruits!: any[];
  @Output() outputValue = new EventEmitter();
  inputName!: string;
  isEmpty: boolean | undefined;
  constructor() {}
  /* Lifecycle hooks of angular */
  ngOnChanges() {}
  ngOnInit(): void {}
  ngDoCheck() {}
  ngAfterContentInit() {}
  ngAfterContentChecked() {}
  ngAfterViewInit() {}
  ngAfterViewChecked() {}
  ngOnDestroy() {}

  submit() {
    this.isEmpty = false;
    if (!this.inputName) {
      this.isEmpty = true;
    } else {
      this.outputValue.emit(this.inputName);
    }
  }
}
