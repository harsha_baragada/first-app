import { TransferService } from './../service/transfer.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  loginForm!: FormGroup;
  fruitsToBeInput: string[] = ['Apple', 'Banana', 'Mango', 'guava', 'litchi'];

  constructor(private transferSevice: TransferService) {}

  ngOnInit(): void {
    this.loginForm = this.createLoginForm();
    this.transferSevice.setData(this.fruitsToBeInput);
  }

  createLoginForm(): FormGroup {
    return new FormGroup({
      userNameFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordFormControl: new FormControl('', Validators.required),
    });
  }

  login() {
    if (this.loginForm.valid) {
      let user = {
        userName: this.loginForm.value.userNameFormControl,
        password: this.loginForm.value.passwordFormControl,
      };
      console.log(user);
    } else {
      alert('form is not valid');
    }
  }

  cancel() {
    this.createLoginForm();
  }

  announceOutput(event: any) {
    alert(event);
  }
}
