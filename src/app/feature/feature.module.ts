import { MatListModule } from '@angular/material/list';
import { UserComponent } from './user/user.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';

@NgModule({
  declarations: [UserComponent],
  imports: [CommonModule, FeatureRoutingModule, MatListModule],
})
export class FeatureModule {}
